import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import styles from '../mainStyle';

const Login = ({navigation}) => {

    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");

    return (
        <View style={styles.container}>
            <View style={styles.loginForm}>
                <View style={styles.loginView}>
                    <View style={styles.InputSection}>
                        <TextInput
                            style={styles.input}
                            placeholder="Username"
                            placeholderTextColor="#878884"
                            value={userName}
                            onChangeText={(username) => { setUserName(username) }}
                        />
                    </View>

                    <View style={styles.InputSection}>
                        <TextInput
                            style={styles.input}
                            placeholder="Password"
                            placeholderTextColor="#878884"
                            value={password}
                            secureTextEntry={true}
                            onChangeText={(Password) => { setPassword(Password) }}
                        />
                    </View>

                    <TouchableOpacity onPress={() => navigation.push('Home', {data: 'from-login'})} style={styles.loginButton}>
                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default Login;