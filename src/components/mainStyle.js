import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    loginForm: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginView: {
        height: 230,
        width: '100%',
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    InputSection: {
        // flex: 1,
        flexDirection: 'row',
        marginTop: 20,
        maxHeight: 45,
        paddingLeft: 10,
        borderRadius: 8,
        backgroundColor: '#dee4ea',
    },
    searchInputSection: {
        flexDirection: 'row',
        marginVertical: 10,
        maxHeight: 45,
        paddingLeft: 10,
        borderRadius: 8,
        backgroundColor: '#dee4ea',
        marginHorizontal: 10
    },
    input: {
        color: '#02bcb1',
        paddingLeft: 10,
        fontSize: 16,
        paddingVertical: 10
    },
    loginButton: {
        paddingVertical: 10,
        paddingHorizontal: 25,
        backgroundColor: '#02bcb1',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        marginTop: 40,
    },
    buttonText: {
        color: '#ffffff', 
        letterSpacing: 0.5, 
        fontWeight: 'bold', 
        fontSize: 16, 
        textTransform: 'uppercase'
    },
    headerView: {
        backgroundColor: '#02bcb1',
        paddingTop: 25,
        alignItems: 'center',
        paddingBottom: 55
    },
    headerText: {
        color: '#ffffff',
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 0.8, 
        textTransform: 'uppercase'
    },
    bodyView: {
        backgroundColor: '#fefefe', 
        marginTop: -30, 
        borderTopLeftRadius: 30, 
        paddingTop: 20, 
        borderTopRightRadius: 30,
        height: '100%',
        paddingBottom: 160
    },
    spinnerTextStyle: {
        color: '#04abbb',
        marginTop: 20,
        // fontFamily: 'monospace',
      },
})