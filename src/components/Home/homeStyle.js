import { StyleSheet, StatusBar } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        // marginTop: StatusBar.currentHeight || 0,
    },
    cardView: {
        shadowColor: '#000',
        shadowOffset: { width: 7, height: 7 },
        shadowOpacity: 0.4,
        shadowRadius: 10,
        borderRadius: 15,
        borderColor: '#c8ccd0',
        borderTopColor: '#c8ccd0',
        elevation: 8,
        marginBottom: 15,
        marginTop: 15,
        backgroundColor: '#02bcb1',
        marginHorizontal: 10
    },
    secondCardView: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 15,
        paddingHorizontal: 10,
        borderTopLeftRadius: 50,
        borderBottomRightRadius: 50,
        paddingVertical: 20,
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    roundId: {
        position: 'absolute',
        top: 3,
        right: 3,
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        backgroundColor: '#02bcb1'
    },
    detailsView: {
        backgroundColor: '#02bcb1',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        borderColor: '#c8ccd0',
        borderWidth: 1,
        elevation: 4,
        borderRadius: 15,
        marginHorizontal: 10,
        marginVertical: 10,
      },
      secondDetailsView: {
        backgroundColor: '#ffffff',
        borderTopLeftRadius: 50,
        borderBottomRightRadius: 50,
        borderRadius: 15,
        paddingVertical: 15,
        paddingTop: 10,
      },
      DetailsCard: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 15,
        flexWrap: 'wrap',
      },
      nameText: {
        fontWeight: 'bold',
        color: '#838682',
        overflow: 'hidden',
        fontSize: 14,
        width: 90
      },
      dotStyle: {
        fontWeight: 'bold',
        color: '#838682',
        overflow: 'hidden',
        fontSize: 14,
      },
      dataText: {
        fontWeight: 'bold',
        color: '#000000',
        // overflow: 'hidden',
        flexWrap: 'wrap',
        // height: 200,
        fontSize: 14
      },
})
