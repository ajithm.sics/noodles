import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, SafeAreaView, TextInput, Image, FlatList } from 'react-native';
import mainStyles from '../mainStyle';
import axios from "react-native-axios";
import styles from './homeStyle';
import Spinner from '../spinner';

const Home = ({ navigation }) => {

    const [restaurant, setRestaurant] = useState([]);
    const [noodlesImage, setNoodlesImage] = useState([]);
    const [imag, setImag] = useState([]);
    const [spinner, setSpinner] = useState(true);
    const [search, setSearch] = useState("");

    useEffect(() => {
        setSpinner(true)
        getRestaurantList();
        getNoodlesImage();
    }, [])

    const getRestaurantList = () => {
        axios.get('https://s3-ap-southeast-1.amazonaws.com/he-public-data/TopRamen8d30951.json')
            .then(function (response) {
                console.log(response);
                setRestaurant(response.data)
                setSpinner(false)
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const getNoodlesImage = () => {
        axios.get('https://s3-ap-southeast-1.amazonaws.com/he-public-data/noodlesec253ad.json')
            .then(function (response) {
                console.log(response);
                setNoodlesImage(response.data)
                setSpinner(false)
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    const renderItem = ({ item }) => (
        <View>
            {noodlesImage.map(itemImage =>
                <TouchableOpacity onPress={() => navigation.navigate('RestaurantDetails', { data: item, image: itemImage.Image })} style={styles.cardView}>
                    <View style={styles.secondCardView}>

                        <View style={{ width: '80%' }}>
                            <View style={{ marginTop: 12, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#838682', width: 90 }}>Brand</Text>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}> :</Text>
                                <Text style={{ marginLeft: 10, fontSize: 13 }}>{item.Brand}</Text>
                            </View>

                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#838682', width: 90 }}>Variety</Text>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}> :</Text>
                                <Text style={{ marginLeft: 10, fontSize: 13 }}>{item.Variety}</Text>
                            </View>

                            <View style={{ marginTop: 7, display: 'flex', flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#838682', width: 90 }}>Country</Text>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}> :</Text>
                                <Text style={{ marginLeft: 10, fontSize: 13 }}>{item.Country}</Text>
                            </View>
                        </View>

                    </View>
                </TouchableOpacity>
            )}
        </View>
    );
    
const searchData = () => {

}

    return (
        <View>
            {spinner ? <Spinner /> :
                <View>
                    <View style={mainStyles.headerView}>
                        <Text style={mainStyles.headerText}>Restaurant List</Text>
                    </View>
                    <View style={mainStyles.bodyView}>

                        <View style={mainStyles.searchInputSection}>
                            <TextInput
                                style={mainStyles.input}
                                placeholder="search"
                                placeholderTextColor="#878884"
                                value={search} 
                                onChangeText={(searchText) => { setSearch(searchText) }}/>
                        </View>

                        <View>
                            <FlatList
                                data={restaurant}
                                renderItem={renderItem}
                                keyExtractor={item => item.id}
                            />
                        </View>
                    </View>
                </View>
            }
        </View>
    );
};

export default Home;
