import React, { useEffect, useState } from "react";
import { View, Text, Image, TouchableOpacity, SafeAreaView, TextInput, FlatList } from 'react-native';
import mainStyles from '../mainStyle';
import axios from "react-native-axios";
import styles from './homeStyle';
import StarRating from 'react-native-star-rating';

const RestaurantDetails = ({ route, navigation }) => {

    const { data, image } = route.params;

    // const images = noodlesImage.map((asset) => asset.Image)
    // const random = Math.floor(Math.random() * images.length)
    // setImag(images[random])
    return (
        <View>
            <View style={mainStyles.headerView}>
                <Text style={mainStyles.headerText}>Restaurant Details</Text>
            </View>
            <View style={mainStyles.bodyView}>

                <View style={styles.detailsView} >
                    <View style={styles.secondDetailsView}>

                        <View style={{ marginLeft: 10, }}>
                            <View style={{ alignItems: 'center', paddingVertical: 10 }}>
                                <Image
                                    source={{ uri: image }}
                                    style={{ width: 100, height: 100 }} />
                            </View>

                            <View style={styles.DetailsCard}>
                                <Text style={styles.nameText}>Brand</Text>
                                <Text style={styles.dotStyle}>:  </Text>
                                <Text style={styles.dataText}>{data.Brand}</Text>
                            </View>

                            <View style={styles.DetailsCard}>
                                <Text style={styles.nameText}>Variety</Text>
                                <Text style={styles.dotStyle}>:  </Text>
                                <Text style={styles.dataText}>{data.Variety}</Text>
                            </View>

                            <View style={styles.DetailsCard}>
                                <Text style={styles.nameText}>Style</Text>
                                <Text style={styles.dotStyle}>:  </Text>
                                <Text style={styles.dataText}>{data.Style}</Text>
                            </View>

                            <View style={styles.DetailsCard}>
                                <Text style={styles.nameText}>Country</Text>
                                <Text style={styles.dotStyle}>:  </Text>
                                <Text style={styles.dataText}>{data.Country}</Text>
                            </View>

                            <View style={{ alignItems: 'center', paddingVertical: 20 }}>
                                <StarRating
                                    disabled={false}
                                    maxStars={5}
                                    rating={data.Stars}
                                    fullStarColor={'#02bcb1'}
                                />
                            </View>

                        </View>
                    </View>
                </View>

            </View>
        </View>
    );
};

export default RestaurantDetails;