import React, { useEffect, useState } from "react";
import { View, Text, Image } from 'react-native';
import mainStyles from './mainStyle';

const Spinner = () => {
    return (
        <View style={{ marginTop: "80%", position: 'absolute', zIndex: 99, width: "100%", paddingHorizontal: 20, height: 70 }}>
            <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', }}>
                <Image
                    source={require('../assets/loader.gif')}
                    style={{ width: 50, height: 50 }} />
                <Text style={mainStyles.spinnerTextStyle}>please wait</Text>
            </View>
        </View>
    );
};

export default Spinner;